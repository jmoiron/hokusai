#!/usr/bin/env python3

"""Hokusai is a simple python3 downloader."""

import re
import os
import sys
import json
import time
import shutil
import urllib
import zipfile
import argparse
import cfscrape
from concurrent.futures import ThreadPoolExecutor, as_completed
from lxml import html

from pprint import pformat

help = """\
Hokusai is based on the concept of URL templating and searching for image
URLs.  It assumes that you can figure out the "full chapter URL" for a
given comic.  It will put chapters in:

$HOKUSAI_ROOT/{name}/{name}-{chapter}.zip

If $HOKUSAI_ROOT is not defined, then it will use the current directory.
If you do not specify a {name}, then it will choose the URL fragment just
before the one with the chapter number template.  If you do not supply
chapter numbers either, it will name the file after the final URL fragment.
"""

options = {}
noact = False
verbose = False
version = "0.1"
root = os.getenv("HOKUSAI_ROOT", ".")


def vprint(*a, **kw):
    if verbose:
        print(*a, **kw)


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--version", action="store_true", help="show verison and exit")
    parser.add_argument("-v", "--verbose", action="store_true", help="show more output")

    subparsers = parser.add_subparsers(dest="command")

    get = subparsers.add_parser("get", description="download chapters")
    # TODO: header
    get.add_argument("-n", "--name", help="series name")
    get.add_argument("-c", "--chapters", help="chapters for templating, eg 1,2,3 or 01-03")
    get.add_argument("--selector", help="CSS selector for image URLs")
    get.add_argument("--regex", help="regex selector for image URLs")
    get.add_argument("--preview", action="store_true", help="show urls but do not download")
    get.add_argument("--delay", type=int, default=0, help="delay between chapter loads in seconds")
    get.add_argument("url", help="base url, use {} for chapter template")

    return parser.parse_args()


def main():
    args = parse_args()

    if args.version:
        print("hokusai v%s" % version)
        return

    options["urls"] = [args.url]
    if args.verbose:
        global verbose
        verbose = True

    if args.name:
        options["name"] = args.name

    # require at least one filter/selector to get image urls
    if args.regex:
        options["regex"] = re.compile(args.regex)
    elif args.selector:
        options["selector"] = args.selector
    else:
        print("hokusai requires one of --regex or --selector to find images")
        sys.exit(-1)

    if args.chapters:
        options["chapters"] = parseRange(args.chapters)
        options["urls"] = [template(args.url, c) for c in options["chapters"]]

    if args.preview:
        global noact
        noact = True

    if args.delay:
        options["delay"] = args.delay

    vprint(pformat(options))
    download(**options)


def template(url, chapter):
    return url.replace("{}", chapter)


def parseRange(s):
    """parseRange takes a string and returns string ranges."""
    r = []
    parts = s.split(",")
    for part in parts:
        if "-" not in part:
            r.append(part)
            continue
        start, end = map(str.strip, part.split("-"))
        fmt = "%%0%dd" % (len(start))
        for i in range(int(start), int(end) + 1):
            r.append(fmt % i)
    return r


def download(urls, name=None, chapters=None, regex=None, selector=None, delay=0):
    """download the chapters at urls.  If chapters is supplied, it must be
    the same length as urls and correspond to each url."""
    names = getNames(urls, name, chapters)
    vprint("names: " + pformat(names))
    scraper = cfscrape.CloudflareScraper()
    if noact:
        return
    first = True
    for name, url in zip(names, urls):
        if delay and not first:
            vprint("sleeping %ds" % (delay))
            time.sleep(delay)
        first = False

        t0 = time.time()
        resp = scraper.get(url)
        vprint("scrape %0.2fs: %s" % (time.time() - t0, url))
        images = matcher(regex, selector, resp.text)
        vprint("found %d images" % (len(images)))
        destdir = os.path.join(root, name)
        prepDir(destdir)
        ref = scraper.headers.get("referer", "")
        scraper.headers.update({"referer": url})
        dlAll(scraper, images, destdir)
        scraper.headers.update({"referer": ref})
        # for i, im in enumerate(images):
        #    dlToDestination(scraper, im, os.path.join(destdir, "%04d" % i))
        mkzip(destdir, "%s.zip" % destdir)
        shutil.rmtree(destdir)


def dlAll(client, images, destdir):
    join = lambda i: os.path.join(destdir, "%04d" % i)
    args = [(im, join(i)) for i, im in enumerate(images)]
    with ThreadPoolExecutor(max_workers=5) as ex:
        futures = {ex.submit(dlToDestination, client, *arg): arg for arg in args}
        done = 0
        for future in as_completed(futures):
            done += 1
        vprint("finished %d" % (done))


def mkzip(src, dest):
    """mkzip zips the directory in src to dest"""
    paths = sorted([os.path.join(src, f) for f in os.listdir(src)])
    with zipfile.ZipFile(dest, "w") as zf:
        for path in paths:
            zf.write(path)


def prepDir(dest):
    if os.path.exists(dest):
        shutil.rmtree(dest)
    os.makedirs(dest)


def dlToDestination(client, url, dest):
    """dlToDestination downloads the url to dest.  The path at dest
    is assumed to not have a proper extension, and one will be chosen
    based on the content type."""
    t0 = time.time()
    resp = client.get(url)
    vprint("get %0.2fs: %s" % (time.time() - t0, url))
    ext = resp.headers["Content-Type"].split("/")[1]
    if ext == "jpeg":
        ext = "jpg"
    destfile = "%s.%s" % (dest, ext)
    with open(destfile, "wb") as f:
        f.write(resp.content)


def matcher(regex, selector, text):
    if regex:
        return regex.findall(text)
    doc = html.document_fromstring(text)
    imgs = doc.cssselect(selector)
    return [img.attrib["src"] for img in imgs]


def getNames(urls, name, chapters):
    if not name:
        name = urllib.parse.urlparse(urls[0]).path.split("/")[-2]
    if name and chapters:
        try:
            ich = map(int, chapters)
            return ["%s-%04d" % (name, c) for c in ich]
        except ValueError:
            return ["%s-%s" % (name, c) for c in chapters]
    purls = [urllib.parse.urlparse(u) for u in urls]
    return [p.path.split("/")[-1] for p in purls]


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
